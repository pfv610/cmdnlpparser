package edu.utsa.cs.sefm.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import edu.stanford.nlp.trees.Tree;
public class NlpParser {

    public static void main(String[] args) {
        StanfordParser spar = new StanfordParser();
        List<Tree> sentenceTreeList;
        BufferedReader br =
                new BufferedReader(new InputStreamReader(System.in));
        String line = null;
        StringBuilder sb = new StringBuilder();
        try {
            while ((line = br.readLine()) != null){
                sb.append(line);
                sb.append("\n");
            }
            sentenceTreeList = spar.parse(sb.toString());
            for (Tree tree : sentenceTreeList){
                System.out.println(tree);
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
